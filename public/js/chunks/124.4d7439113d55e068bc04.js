(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[124],{

/***/ "./frontend/src/@core/layouts/components/Logo.vue":
/*!********************************************************!*\
  !*** ./frontend/src/@core/layouts/components/Logo.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Logo_vue_vue_type_template_id_556c28d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Logo.vue?vue&type=template&id=556c28d4& */ "./frontend/src/@core/layouts/components/Logo.vue?vue&type=template&id=556c28d4&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Logo_vue_vue_type_template_id_556c28d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Logo_vue_vue_type_template_id_556c28d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/src/@core/layouts/components/Logo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/src/@core/layouts/components/Logo.vue?vue&type=template&id=556c28d4&":
/*!***************************************************************************************!*\
  !*** ./frontend/src/@core/layouts/components/Logo.vue?vue&type=template&id=556c28d4& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logo_vue_vue_type_template_id_556c28d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Logo.vue?vue&type=template&id=556c28d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/layouts/components/Logo.vue?vue&type=template&id=556c28d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logo_vue_vue_type_template_id_556c28d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logo_vue_vue_type_template_id_556c28d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./frontend/src/assets/images/pages/coming-soon-dark.svg":
/*!***************************************************************!*\
  !*** ./frontend/src/assets/images/pages/coming-soon-dark.svg ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/_/_/_/_/damskart-admin-git/frontend/src/assets/images/pages/coming-soon-dark.svg";

/***/ }),

/***/ "./frontend/src/assets/images/pages/coming-soon.svg":
/*!**********************************************************!*\
  !*** ./frontend/src/assets/images/pages/coming-soon.svg ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/_/_/_/_/damskart-admin-git/frontend/src/assets/images/pages/coming-soon.svg";

/***/ }),

/***/ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue":
/*!***************************************************************!*\
  !*** ./frontend/src/views/pages/miscellaneous/ComingSoon.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ComingSoon_vue_vue_type_template_id_bfeab69a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ComingSoon.vue?vue&type=template&id=bfeab69a& */ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=template&id=bfeab69a&");
/* harmony import */ var _ComingSoon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ComingSoon.vue?vue&type=script&lang=js& */ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ComingSoon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ComingSoon.vue?vue&type=style&index=0&lang=scss& */ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ComingSoon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ComingSoon_vue_vue_type_template_id_bfeab69a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ComingSoon_vue_vue_type_template_id_bfeab69a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/src/views/pages/miscellaneous/ComingSoon.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ComingSoon.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************!*\
  !*** ./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ComingSoon.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=template&id=bfeab69a&":
/*!**********************************************************************************************!*\
  !*** ./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=template&id=bfeab69a& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_template_id_bfeab69a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ComingSoon.vue?vue&type=template&id=bfeab69a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=template&id=bfeab69a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_template_id_bfeab69a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ComingSoon_vue_vue_type_template_id_bfeab69a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bootstrap-vue */ "./frontend/node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var _core_layouts_components_Logo_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/layouts/components/Logo.vue */ "./frontend/src/@core/layouts/components/Logo.vue");
/* harmony import */ var _store_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/store/index */ "./frontend/src/store/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* eslint-disable global-require */



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VuexyLogo: _core_layouts_components_Logo_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    BLink: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BLink"],
    BForm: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BForm"],
    BButton: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BButton"],
    BFormInput: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BFormInput"],
    BImg: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BImg"]
  },
  data: function data() {
    return {
      downImg: __webpack_require__(/*! @/assets/images/pages/coming-soon.svg */ "./frontend/src/assets/images/pages/coming-soon.svg")
    };
  },
  computed: {
    imgUrl: function imgUrl() {
      if (_store_index__WEBPACK_IMPORTED_MODULE_2__["default"].state.appConfig.layout.skin === 'dark') {
        // eslint-disable-next-line vue/no-side-effects-in-computed-properties
        this.downImg = __webpack_require__(/*! @/assets/images/pages/coming-soon-dark.svg */ "./frontend/src/assets/images/pages/coming-soon-dark.svg");
        return this.downImg;
      }

      return this.downImg;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".misc-wrapper {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-preferred-size: 100%;\n      flex-basis: 100%;\n  min-height: 100vh;\n  width: 100%;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.misc-wrapper .misc-inner {\n  position: relative;\n  max-width: 750px;\n}\n.misc-wrapper .brand-logo {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  position: absolute;\n  top: 2rem;\n}\n[dir] .misc-wrapper .brand-logo {\n  margin: 0;\n}\n[dir=ltr] .misc-wrapper .brand-logo {\n  left: 2rem;\n}\n[dir=rtl] .misc-wrapper .brand-logo {\n  right: 2rem;\n}\n.misc-wrapper .brand-logo .brand-text {\n  font-weight: 600;\n}\n.brand-logo svg {\n  height: 28px;\n  width: 40.95px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ComingSoon.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/@core/layouts/components/Logo.vue?vue&type=template&id=556c28d4&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/@core/layouts/components/Logo.vue?vue&type=template&id=556c28d4& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "svg",
    {
      attrs: {
        version: "1.0",
        xmlns: "http://www.w3.org/2000/svg",
        width: "80.000000pt",
        height: "65.000000pt",
        viewBox: "0 0 80.000000 65.000000",
        preserveAspectRatio: "xMidYMid meet"
      }
    },
    [
      _c(
        "g",
        {
          attrs: {
            transform:
              "translate(0.000000,65.000000) scale(0.100000,-0.100000)",
            fill: "#000000",
            stroke: "none"
          }
        },
        [
          _c("path", {
            attrs: {
              d:
                "M336 625 c-11 -8 -32 -15 -48 -15 -17 0 -31 -7 -38 -20 -6 -11 -21\n\t\t-20 -34 -20 -17 0 -27 -9 -36 -30 -7 -16 -21 -32 -31 -35 -10 -4 -19 -14 -19\n\t\t-24 0 -10 -9 -30 -21 -45 -15 -19 -19 -34 -15 -52 4 -15 2 -32 -5 -40 -7 -9\n\t\t-8 -24 -3 -41 5 -16 8 -41 9 -58 0 -27 4 -30 34 -33 31 -3 33 -2 27 20 -30\n\t\t107 -9 196 62 267 108 108 266 108 374 0 70 -70 96 -185 62 -272 -6 -16 -2\n\t\t-18 27 -15 32 3 34 5 34 39 1 20 6 43 13 51 9 12 9 19 -3 34 -9 12 -12 31 -9\n\t\t50 4 21 1 34 -10 43 -9 8 -16 24 -16 37 0 16 -8 28 -25 36 -13 6 -28 24 -34\n\t\t39 -6 18 -17 29 -29 29 -10 0 -27 9 -37 20 -10 11 -30 20 -44 20 -15 0 -36 7\n\t\t-48 15 -15 11 -29 12 -45 6 -13 -4 -32 -5 -43 0 -27 10 -27 10 -49 -6z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M322 488 c3 -20 9 -23 47 -23 27 0 46 -5 48 -12 3 -9 -10 -13 -46\n\t\t-13 -39 0 -51 -4 -51 -15 0 -11 12 -15 51 -15 45 0 50 -2 39 -15 -7 -9 -29\n\t\t-15 -52 -15 -50 0 -48 -11 12 -84 30 -36 53 -55 68 -56 31 0 28 15 -13 65\n\t\tl-36 43 24 6 c13 3 29 17 35 31 6 14 18 25 27 25 24 0 18 27 -7 34 l-23 6 23\n\t\t15 c12 9 22 23 22 31 0 11 -18 14 -86 14 -83 0 -85 -1 -82 -22z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M122 148 l1 -43 4 38 c4 39 13 46 32 23 10 -11 10 -20 1 -38 -12 -22\n\t\t-11 -22 6 -6 15 14 16 21 6 43 -7 15 -20 25 -32 25 -17 0 -20 -6 -18 -42z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M270 182 c0 -7 7 -20 15 -28 19 -20 20 -4 0 22 -9 12 -14 14 -15 6z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M329 163 c-8 -18 -17 -33 -21 -33 -4 0 -8 -4 -8 -10 0 -19 19 -10 26\n\t\t13 4 12 11 25 17 28 5 4 7 13 4 20 -3 9 -10 2 -18 -18z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M377 183 c-13 -13 -7 -32 12 -38 11 -3 22 -14 24 -23 4 -16 5 -16 6\n\t\t0 0 10 -8 22 -19 28 -26 14 -26 34 0 27 11 -3 18 -1 14 4 -6 11 -28 12 -37 2z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M442 150 c0 -25 2 -37 5 -27 5 21 22 22 29 2 4 -8 11 -15 17 -15 5 0\n\t\t2 9 -8 20 -14 16 -16 23 -7 32 7 7 12 17 12 22 0 6 -8 -1 -17 -14 l-17 -24 -7\n\t\t24 c-5 19 -7 14 -7 -20z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M525 157 c-19 -45 -19 -51 -1 -35 8 7 12 17 9 21 -3 4 1 15 8 23 6 8\n\t\t9 17 6 21 -4 3 -14 -10 -22 -30z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M600 180 c12 -8 13 -13 3 -29 -10 -16 -9 -22 5 -33 15 -11 16 -11 4\n\t\t3 -10 14 -10 18 2 22 23 9 16 47 -9 47 -16 0 -17 -2 -5 -10z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M657 182 c6 -4 13 -23 16 -42 l4 -35 1 37 c1 20 8 39 15 41 6 3 -1 6\n\t\t-18 6 -17 0 -25 -3 -18 -7z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M201 153 c-1 -6 -4 -20 -7 -30 -5 -17 -5 -17 6 0 6 10 9 23 6 30 -3\n\t\t9 -5 9 -5 0z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M228 149 c-3 -8 1 -20 8 -27 15 -14 17 -7 5 22 -6 15 -9 16 -13 5z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M272 130 c0 -14 2 -19 5 -12 2 6 2 18 0 25 -3 6 -5 1 -5 -13z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M342 130 c0 -14 2 -19 5 -12 2 6 2 18 0 25 -3 6 -5 1 -5 -13z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M556 125 c4 -8 10 -15 15 -15 4 0 6 7 3 15 -4 8 -10 15 -15 15 -4 0\n\t\t-6 -7 -3 -15z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M370 121 c0 -5 7 -12 16 -15 14 -5 15 -4 4 9 -14 17 -20 19 -20 6z"
            }
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d:
                "M218 65 c18 -9 35 -22 38 -30 4 -9 19 -15 39 -15 19 0 36 -4 39 -9 7\n\t\t-11 135 -11 142 0 3 5 20 9 39 9 20 0 35 6 39 15 3 8 20 21 38 30 32 14 31 15\n\t\t-42 9 -95 -7 -195 -7 -290 0 -73 6 -74 5 -42 -9z"
            }
          })
        ]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=template&id=bfeab69a&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/pages/miscellaneous/ComingSoon.vue?vue&type=template&id=bfeab69a& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "misc-wrapper" },
    [
      _c(
        "b-link",
        { staticClass: "brand-logo" },
        [
          _c("vuexy-logo"),
          _vm._v(" "),
          _c("h2", { staticClass: "brand-text text-primary ml-1" }, [
            _vm._v("\n        Vuexy\n      ")
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "misc-inner p-2 p-sm-3" }, [
        _c(
          "div",
          { staticClass: "w-100 text-center" },
          [
            _c("h2", { staticClass: "mb-1" }, [
              _vm._v("\n          We are launching soon 🚀\n        ")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-3" }, [
              _vm._v(
                "\n          We're creating something awesome. Please subscribe to get notified when it's ready!\n        "
              )
            ]),
            _vm._v(" "),
            _c(
              "b-form",
              {
                staticClass: "row justify-content-center m-0 mb-2",
                attrs: { inline: "" },
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [
                _c("b-form-input", {
                  staticClass: "col-12 col-md-5 mb-1 mr-md-2",
                  attrs: {
                    id: "notify-email",
                    type: "text",
                    placeholder: "john@example.com"
                  }
                }),
                _vm._v(" "),
                _c(
                  "b-button",
                  {
                    staticClass: "mb-1 btn-sm-block",
                    attrs: { variant: "primary", type: "submit" }
                  },
                  [_vm._v("\n            Notify\n          ")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("b-img", {
              attrs: { fluid: "", src: _vm.imgUrl, alt: "Coming soon page" }
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);