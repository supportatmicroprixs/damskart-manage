<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use \Venturecraft\Revisionable\RevisionableTrait;
use Laravel\Passport\HasApiTokens;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, RevisionableTrait;
    protected $revisionEnabled = true;

    use CrudTrait;
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'state_id',
		'district_id',
		'created_by',
		'first_name',
		'last_name',
		'email',
		'mobile',
		'password',
		'gender',
		'address',
		'district',
		'state',
		'pin_code',
		'profile_pic',
		'aadhar_number',
		'pan_number',
		'company_name',
		'comapny_logo',
		'company_address',
		'company_pan',
		'package',
		'domain',
		'ewallet',
		'aeps_wallet',
		'fcm_token',
		'pin',
		'otp',
		'last_login',
		'ewallet_status',
		'aeps_status',
		'fcm_status',
		'online',
		'role',
		'verify',
		'status',
		'profile_status',
		'onboarding',
		'fundtransfer',
		'yesbank',
		'kyc_update',
		'refer_by',
		'aadhar_pic_front',
		'pan_pic',
		'GpID',
		'GpStatus',
		'BcID',
		'dob',
		'notification',
		'passbook_image',
		'marksheet_10',
		'marksheet_12',
		'digio_document',
		'digio_kyc_status',
		'digio_response',
		'digio_e_sign_status',
		'package_id',
		'allow_add_member',
		'aeps_ekyc_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
