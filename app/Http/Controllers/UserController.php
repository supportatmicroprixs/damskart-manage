<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function loginUser(Request $request){
        if (Auth::attempt(['status' => '1', 'mobile' => $request->mobile, 'password' => $request->password, 'domain' => url('/') ])) {
            $retailer = Auth::user();

            $token =  $retailer->createToken('token')->accessToken;
            $token1 =  $retailer->createToken('token')->accessToken;

            $json = array('userData' => $retailer, 'accessToken' => $token, 'refreshToken' => $token1);

            return response()->json($json, 200);
        }
        else
        {
            $json = array('error' => ['mobile' => ['Mobile or Password is Invalid']]);

            return response()->json($json, 400);
        }
    }
}